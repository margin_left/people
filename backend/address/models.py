from gettext import gettext as _

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class Address:
    country = models.ForeignKey('Countries', on_delete=models.RESTRICT)
    region = models.CharField(max_length=45)  # Край, область, штат и т.п. регион
    city_or_district = models.CharField(max_length=40)  # название города или сельский район
    village_or_district = models.CharField(max_length=40)  # сельское поселение или район города
    house_number = models.CharField(max_length=8)  # номер дома
    block = models.CharField(max_length=3)  # корпус
    entrance = models.CharField(max_length=2)  # номер подъезда
    floor = models.IntegerField(validators=[MaxValueValidator(200), MinValueValidator(-6)])  # номер этажа
    apartment = models.CharField(max_length=5)  # комната или номер кабинета
    room = models.CharField(max_length=5)  # номер помещения
    postal_code = models.IntegerField(validators=[MaxValueValidator(999000), MinValueValidator(0)])


class Countries(models.Model):
    country_name = models.CharField(max_length=32)
    phone_code = models.CharField(max_length=7)
    start_date = models.DateField()
    end_date = models.DateField()


class RussianRegions(models.Model):
    class RegionTypes(models.TextChoices):
        REPUBLIC = _('Republic')
        KRAI = _('Krai')
        OBLAST = _('Oblast')
        FED_CITY = _('Federal city')
        AUTONOM_OBLAST = _('Autonomous oblast')
        AUTONOM_OKRUG = _('Autonomous okrug')

    region_name = models.CharField(max_length=45)
    zip_code = models.IntegerField(validators=[MaxValueValidator(999000), MinValueValidator(100)])
    region_type = models.CharField(choices=RegionTypes.choices)
