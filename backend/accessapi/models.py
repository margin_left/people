from django.db import models

from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    avatar = models.FileField(null=True, blank=True, upload_to='avatars/',
                              verbose_name='Profile picture')

    def __str__(self):
        return self.username
