from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from address.models import Countries


class Person(models.Model):
    surname = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    patronym = models.CharField(max_length=200)
    middle_names = models.CharField(max_length=200)
    known_by = models.CharField(max_length=200)


class PhoneNumber(models.Model):
    country_code = models.ForeignKey(Countries, on_delete=models.RESTRICT)
    operator_code = models.IntegerField(validators=[MaxValueValidator(99999), MinValueValidator(1)])
    number = models.IntegerField(validators=[MaxValueValidator(9999999), MinValueValidator(10000)])
